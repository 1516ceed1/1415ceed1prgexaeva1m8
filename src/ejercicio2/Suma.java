/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

import java.util.Scanner;

/**
 *
 * @author Paco Aldarias<paco.aldarias@ceedcv.es>
 * @author 13-ene-2016
 */
public class Suma {

    static void total(int contador[]) {
        int numero = 1;
        String linea;
        Scanner sc = new Scanner(System.in);

        while (numero != 0) {
            System.out.print("Numero: ");
            linea = sc.nextLine();
            try {
                numero = Integer.parseInt(linea);
                contador[0] = contador[0] + numero;
            } catch (Exception e) {
                System.out.println("Error");
            }
        }
    }

    public static void main(String[] args) {

        int suma[] = {0};
        total(suma);
        System.out.println("La suma es " + suma[0]);

    }

}
