/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

import java.util.Scanner;

/**
 *
 * @author Paco Aldarias<paco.aldarias@ceedcv.es>
 * @author 13-ene-2016
 */
public class Palabras {

    static void total(int contador[]) {

        String linea = "";
        Scanner sc = new Scanner(System.in);

        while (!linea.equals("fin")) {
            System.out.print("Palabra: ");
            linea = sc.nextLine();
            contador[0]++;
        }
        contador[0]--;

    }

    public static void main(String[] args) {

        int contador[] = {1};
        total(contador);
        System.out.println("Total Palabras: " + contador[0]);

    }

}
