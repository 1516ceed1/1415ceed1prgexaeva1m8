package controlador;

import java.io.IOException;
import modelo.IModelo;
import vista.Vista;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-oct-2014
 */
public class Main {

   /**
    * @param args the command line arguments
    * @throws java.io.IOException
    */
   public static void main(String[] args) throws IOException {

      // Declaramos el modelo.
      IModelo modelo = null;

      // Declaramos la vista y creamos objeto vista
      Vista vista = new Vista();

      // Declaramos y creamos un objeto controlador.
      Controlador controlador = new Controlador(modelo, vista);

   }

}
