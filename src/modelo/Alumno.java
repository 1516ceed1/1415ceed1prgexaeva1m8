/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 * Fichero: Alumno.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-oct-2014
 */
public class Alumno extends Persona {

   private String nia;

   /**
    * @return the nie
    */
   public String getNia() {
      return nia;
   }

   /**
    * @param nie the nie to set
    */
   public void setNia(String nia) {
      this.nia = nia;
   }

   public String toString() {
      return getId() + " " + getNombre() + " " + getEdad() + " "
              + getEmail() + " " + getNia() + " " + getGrupo();
   }

}
